import express from 'express';
import mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

import user from './routes/user';
import User from './user';
import tokenProtection from './middleware/tokenProtected';

const secret = process.env.SUPER_SECRET_KEY ? process.env.SUPER_SECRET_KEY : '';

if (secret === '') {
  console.log('internal error');
  process.exit(1);
}

mongoose.connect('mongodb://localhost:27017/rallydb', { useNewUrlParser: true })
  .then(() => console.log('connected'))
  .catch(() => {
    console.log('Database unreachable');
    process.exit(1);
  });

// Create a new express application instance
const app = express();
app.use(bodyParser.json());

app.use('/users', user);

app.post('/login', (req, res) => {
  const { userName, password } = req.body;
  User.findOne({ userName })
    .then(result => {
      if (result) {
        const { password: pw } = result;
        return bcrypt.compare(password, pw);
      }
      return Promise.reject(Error('unauthorized'));
    })
    .then(val => {
      if (val) {
        const token = jwt.sign({
          data: { userName },
        }, secret, { expiresIn: '1h' });
        res.send({ token });
      } else {
        res.status(401).send({ message: 'login failed' });
      }
    })
    .catch(() => res.status(401).send({ message: 'login failed' }));
});

app.post('/a', tokenProtection, (req, res) => res.send({ message: 'yay' }));

app.get('/self', tokenProtection, (req, res) => res.send({ userName: res.locals }));

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
