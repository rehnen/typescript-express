import { Schema, model, Document } from 'mongoose';

export interface IUser extends Document {
  email: string;
  userName: string;
  password: string;
}

const userSchema = new Schema({
  email: { type: String, require: true },
  userName: { type: String, require: true },
  password: { type: String, require: true },
});

const User = model<IUser>('users', userSchema);

export default User;
