import express from 'express';
import jwt from 'jsonwebtoken';

const secret = process.env.SUPER_SECRET_KEY;

const checkToken = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const { authorization } = req.headers;
  if (!authorization) {
    res.status(401).send({ message: 'unauthorized' });
  } else {
    const token = authorization.split(' ')[1];
    jwt.verify(token, secret, (err, val) => {
      if (err) {
        res.status(401).send({ message: 'unauthorized' });
      } else {
        res.locals.user = val.data.userName;
        next();
      }
    });
  }
};

export default checkToken;
