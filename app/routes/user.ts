import express from 'express';
import bcrypt from 'bcryptjs';
import User from '../user';

const router = express.Router();

router.get('/', (req, res) => {
  User.find()
    .select('email userName -_id')
    .then(result => res.send(result))
    .catch(() => res.status(404).send());
});

router.get('/:userName', (req, res) => {
  const { userName } = req.params;
  User.findOne({ userName })
    .then(result => {
      if (!result) {
        res.status(404).send();
      } else {
        res.send(result);
      }
    })
    .catch(() => res.status(404).send());
});

router.post('/', (req, res) => {
  const { email, userName, password } = req.body;
  bcrypt.hash(password, 10)
    .then(pw => User.create({ email, userName, password: pw }))
    .then(() => {
      res.send({ email, userName });
    }).catch(() => {
      res.status(500).send({ message: 'you cannot perform simplest task, you will not get passing grade' });
    });
});

router.put('/:userName', (req, res) => {
  const { userName: reqUserName } = req.params;
  const { userName, email } = req.body;
  User.findOneAndUpdate({ userName: reqUserName }, { userName, email })
    .then(result => res.send(result))
    .catch(err => res.status(404).send(err));
});

router.delete('/:userName', (req, res) => {
  const { userName } = req.params;
  User.findOneAndDelete({ userName })
    .then(result => res.send(result))
    .catch(err => res.status(404).send(err));
});

export default router;
